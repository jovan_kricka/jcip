package com.jcip.session5;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * <p>Executor service with fixed thread pool is created with pool size of 10.</p>
 * <p>We submit 50 tasks to the service for execution.</p>
 *
 * @author Jovan Kricka
 * @since 2019-08-09.
 */
public class ExecutorServiceExample {

	private static final int NUMBER_OF_THREADS = 10;
	private static final ExecutorService executorService = Executors.newFixedThreadPool(NUMBER_OF_THREADS);

	public static void main(String[] args) {

		for (int i = 0; i < 50; i++) {

			final int taskId = i;
			executorService.execute(() -> {

				System.out.println("Task " + taskId + " is processed by thread " + Thread.currentThread().getName() + ".");

				try {
					Thread.sleep((long) (Math.random() * 5_000));
				} catch (InterruptedException e) {
					e.printStackTrace();
				}

			});

		}

		// why program is not terminating??

	}

}
