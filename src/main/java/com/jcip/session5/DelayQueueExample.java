package com.jcip.session5;

import java.util.concurrent.DelayQueue;
import java.util.concurrent.Delayed;
import java.util.concurrent.TimeUnit;

/**
 * @author Jovan Kricka
 * @since 2019-08-21.
 */
public class DelayQueueExample {

	private static final DelayQueue<Delayed> avocados = new DelayQueue<>();

	public static void main(String[] args) {

		for (int i = 0; i < 50; i++) {
			avocados.add(new Avocado((long) (Math.random() * 5_000)));
		}

		for (int i = 0; i < 50; i++) {

			final int threadIdentifier = i;
			new Thread(() -> {

				try {

					Thread.sleep((long) (Math.random() * 3_000));
					System.out.println("I am thread " + threadIdentifier + ", woke up and I want an avocado!");
					final Avocado avocado = (Avocado) avocados.take();
					System.out.println("I had to wait for " + avocado.ripeTime + ", but I got my avocado finally!");

				} catch (InterruptedException e) {
					e.printStackTrace();
				}

			}).start();

		}

	}

	private static class Avocado implements Delayed {

		private long ripeTime;
		private long startTime;

		public Avocado(final long ripeTime) {

			this.ripeTime = ripeTime;
			this.startTime = System.currentTimeMillis() + ripeTime;

		}

		public long getRipeTime() {
			return ripeTime;
		}

		@Override
		public long getDelay(TimeUnit unit) {

			long diff = startTime - System.currentTimeMillis();
			return unit.convert(diff, TimeUnit.MILLISECONDS);

		}

		@Override
		public int compareTo(final Delayed avocado) {
			return (int) (this.ripeTime - ((Avocado) avocado).ripeTime);
		}

	}

}
