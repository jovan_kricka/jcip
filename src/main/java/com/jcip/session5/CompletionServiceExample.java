package com.jcip.session5;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * @author Jovan Kricka
 * @since 2019-08-21.
 */
public class CompletionServiceExample {

	private static final int NUMBER_OF_COOKIES_AND_CHILDREN = 10;
	private static final ExecutorService EXECUTOR_SERVICE = Executors.newFixedThreadPool(2);
	private static final CompletionService table = new ExecutorCompletionService(EXECUTOR_SERVICE);

	public static void main(final String[] args) throws InterruptedException {

		final Thread grandma = new Thread(() -> {

			System.out.println("I will start baking some cookies!");

			for (int i = 0; i < NUMBER_OF_COOKIES_AND_CHILDREN; i++) {

				// bake a cookie
				final int cookieIdentifier = i;
				final Callable cookie = () -> {

					try {
						Thread.sleep((long) (Math.random() * 5_000)); // cooling off
					} catch (InterruptedException e) {
						e.printStackTrace();
					}

					return cookieIdentifier;
				};

				// put it on the table to cool off
				table.submit(cookie);
				System.out.println("Cookie " + cookieIdentifier + " baked, I've put it on table to cool off. Will sleep a bit now... zzzz...");
				try {
					Thread.sleep((long) (Math.random() * 5_000));
				} catch (InterruptedException e) {
					e.printStackTrace();
				}

			}

		});

		final List<Thread> children = new ArrayList<>();
		for (int i = 0; i < NUMBER_OF_COOKIES_AND_CHILDREN; i++) {

			children.add(new Thread(() -> {

				final Future cookie;
				try {

					System.out.println("Will wait for cookie to cool off...");
					cookie = table.take();
					final int cookieIdentifier = (int) cookie.get();
					System.out.println("Finally I can eat my cookie (" + cookieIdentifier + ")!");

				} catch (InterruptedException | ExecutionException e) {
					e.printStackTrace();
				}

			}));

		}

		children.forEach(Thread::start);
		grandma.start();

		grandma.join();
		for (Thread child : children) {
			child.join();
		}

		EXECUTOR_SERVICE.shutdown();

	}

}
