package com.jcip.session2;

/**
 * <p>When object is being instantiated thread is created and started that has access to the not yet constructed object.</p>
 * <p>This causes it to print 0 instead of 42.</p>
 *
 * @author Jovan Kricka
 * @since 2019-08-09.
 */
public class NotProperlyConstructedObject {

	private final int number;

	public NotProperlyConstructedObject() throws InterruptedException {

		new Thread(this::doSomething).start();
		Thread.sleep(1000);
		number = 42;

	}

	private void doSomething() {
		System.out.println(number);
	}

	public static void main(String args[]) throws InterruptedException {

		new NotProperlyConstructedObject();

	}

}
