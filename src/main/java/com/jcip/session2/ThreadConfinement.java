package com.jcip.session2;

/**
 * <p>Multiple threads are accessing same {@link ThreadLocal} variable.</p>
 * <p>Each of those threads will set its own value, sleep for one second and then get its own value.</p>
 *
 * @author Jovan Kricka
 * @since 2019-08-09.
 */
public class ThreadConfinement {

	private ThreadLocal<Integer> number = new ThreadLocal<>();

	public void setNumber(final int number) {
		this.number.set(number);
	}

	public Integer getNumber() {
		return this.number.get();
	}

	public static void main(final String[] args) {

		final ThreadConfinement sharedObject = new ThreadConfinement();

		for (int i = 0; i < 10; i++) {

			final int threadIdentifier = i;
			new Thread(() -> {

				System.out.println("I am thread " + threadIdentifier + " and I am writing this number: " + (50 + threadIdentifier));
				sharedObject.setNumber(50 + threadIdentifier);

				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}

				System.out.println("I am thread " + threadIdentifier + " and I retrieved number: " + sharedObject.getNumber());

			}).start();

		}

	}

}
