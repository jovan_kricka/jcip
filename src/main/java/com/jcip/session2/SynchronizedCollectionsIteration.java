package com.jcip.session2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.ConcurrentModificationException;
import java.util.List;

/**
 * <p>We have two threads accessing shared synchronized collection. One writes to the collection one will iterate over it</p>
 * <p>If there is no synchronization on iteration we will get {@link ConcurrentModificationException}.</p>
 *
 * @author Jovan Kricka
 * @since 2019-08-09.
 */
public class SynchronizedCollectionsIteration {

	private static final List<Integer> synchronizedCollection = Collections.synchronizedList(new ArrayList<>());

	public static void main(String[] args) {

		// add 100 numbers
		for (int i = 0; i < 100; i++) {
			synchronizedCollection.add(i);
		}

		// start thread that adds numbers
		new Thread(() -> {

			for (int i = 0; i < 100; i++) {
				System.out.println("Adding " + i);
				//				synchronized (synchronizedCollection) {
				synchronizedCollection.add(i);
				//				}
			}

		}).start();

		// start thread that iterates collection
		new Thread(() -> {

						synchronized (synchronizedCollection) {
			for (Integer number : synchronizedCollection) {
				System.out.println("Getting : " + number);
				try {
					Thread.sleep(30);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
						}

		}).start();

	}

}