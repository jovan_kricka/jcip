package com.jcip.session1;

public class StaleDataAndReordering {

	private static boolean ready;
	private static int number;

	private static class ReaderThread extends Thread {

		public void run() {

			while (!ready) {
				Thread.yield();
			}

			System.out.println(number);

		}

	}

	/**
	 * 3 possible outcomes of the program are:
	 * <li>42</li>
	 * <li>0</li>
	 * <li>Program never terminates</li>
	 *
	 * @param args
	 */
	public static void main(String[] args) {

		new ReaderThread().start();
		number = 42;
		ready = true;

	}

}
