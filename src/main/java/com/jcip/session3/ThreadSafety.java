package com.jcip.session3;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Class state is composed of two variables:
 * <ul>
 * <li>cache</li> and
 * <li>counter.</li>
 * </ul>
 * <p>Both of objects of these two fields are thread safe.</p>
 * <p>Method {@link SimpleCache#update(String)} adds new {@link CacheEntry} with given text and next
 * sequence number to the cache.</p>
 * <p>{@link AtomicInteger} is used instead of volatile int because otherwise some sequence numbers could be swallowed.</p>
 * <p>Cache entries that are stored in the list are not stored by the order of their sequence numbers (which is probably not required).</p>
 *
 * @author Jovan Kricka
 * @since 2019-08-02.
 */
public class ThreadSafety {

	public class SimpleCache {

		private List<CacheEntry> cache = new CopyOnWriteArrayList<>();
		private AtomicInteger counter = new AtomicInteger(0);

		public void update(String text) {
			cache.add(new CacheEntry(text, counter.getAndIncrement()));
		}

		public List<CacheEntry> getContent() {
			return Collections.unmodifiableList(cache);
		}
	}

	/**
	 * Class only has constructor, getters, equality methods and toString method (no setters). So this class is immutable.
	 */
	public class CacheEntry {

		private String text;
		private int sequenceNr;

		public CacheEntry(String text, int sequenceNr) {
			this.text = text;
			this.sequenceNr = sequenceNr;
		}

		public String getText() {
			return text;
		}

		public int getSequenceNr() {
			return sequenceNr;
		}

		@Override
		public int hashCode() {
			return Objects.hash(text, sequenceNr);
		}

		@Override
		public boolean equals(Object obj) {
			if (obj == this) {
				return true;
			}
			if (!(obj instanceof CacheEntry)) {
				return false;
			}
			CacheEntry other = (CacheEntry) obj;
			return Objects.equals(text, other.text)
				&& sequenceNr == other.sequenceNr;
		}

		@Override
		public String toString() {
			return text + " #" + sequenceNr;
		}

	}

	public void runTest() throws InterruptedException {

		final SimpleCache simpleCache = new SimpleCache();

		final List<Thread> myThreads = createThreads(simpleCache, 100);
		startThreads(myThreads);
		waitForThreadsToFinish(myThreads);

		final String listAsString = simpleCache.getContent().toString();

		final Pattern pattern = Pattern.compile("#([0-9]+),");
		final Matcher matcher = pattern.matcher(listAsString);
		int counter = 0;

		while (matcher.find()) {

			final String nextSequenceNumber = matcher.group(1);

			if (!(Integer.toString(counter++)).equals(nextSequenceNumber)) {

				System.out.println(listAsString);
				throw new AssertionError("Expected next sequence number in the list to be " + (counter - 1) + ", but it is " + nextSequenceNumber);

			}

		}

	}

	private void waitForThreadsToFinish(final List<Thread> myThreads) throws InterruptedException {

		for (final Thread myThread : myThreads) {
			myThread.join();
		}

	}

	private void startThreads(final List<Thread> myThreads) {

		for (final Thread myThread : myThreads) {
			myThread.start();
		}

	}

	private List<Thread> createThreads(final SimpleCache simpleCache, final int numberOfThreadsToCreate) {

		final List<Thread> myThreads = new ArrayList<>();

		for (int i = 0; i < numberOfThreadsToCreate; i++) {

			final int threadNumber = i;
			myThreads.add(new Thread(() -> {

				for (int j = 0; j < 100; j++) {
					simpleCache.update("Chandrayan " + threadNumber + "_" + j);
				}
			}));

		}

		return myThreads;

	}

	public static void main(String[] args) throws InterruptedException {

		new ThreadSafety().runTest();

	}

}
