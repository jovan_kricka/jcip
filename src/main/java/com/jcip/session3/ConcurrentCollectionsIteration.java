package com.jcip.session3;

import java.util.ConcurrentModificationException;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * <p>We have two threads accessing shared concurrent collection. One writes to the collection one will iterate over it</p>
 * <p>We can not get {@link ConcurrentModificationException}.</p>
 *
 * @author Jovan Kricka
 * @since 2019-08-09.
 */
public class ConcurrentCollectionsIteration {

	private static final List<Integer> concurrentCollection = new CopyOnWriteArrayList<>();

	public static void main(String[] args) {

		// add 100 numbers
		for (int i = 0; i < 100; i++) {
			concurrentCollection.add(i);
		}

		// start thread that adds numbers
		new Thread(() -> {

			for (int i = 0; i < 100; i++) {
				System.out.println("Adding " + i);
				concurrentCollection.add(i);
			}

		}).start();

		// start thread that iterates collection
		new Thread(() -> {

			for (Integer number : concurrentCollection) {
				System.out.println("Getting : " + number);
				try {
					Thread.sleep(30);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}

		}).start();

	}

}
