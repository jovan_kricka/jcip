package com.jcip.session3;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * <p>We are going to run two parallel tests.</p>
 * <p>Each test will create 4 threads and start them.</p>
 * <p>Every thread is going to first add 1k elements to the map, then remove 1k elements from the map and then update progress.</p>
 * <p>One set of threads will use synchronized map and other concurrent map.</p>
 * <p>Progress of each map will be continuously printed out.</p>
 *
 * @author Jovan Kricka
 * @since 2019-08-09.
 */
public class ConcurrentVsSynchronized {

	private static final Map<String, String> synchronizedMap = Collections.synchronizedMap(new HashMap<>());
	private static AtomicInteger synchronizedMapProgressCounter = new AtomicInteger();
	private static final Map<String, String> concurrentMap = new ConcurrentHashMap<>();
	private static AtomicInteger concurrentMapProgressCounter = new AtomicInteger();

	public static void main(String[] args) {

		testThreads(synchronizedMap, synchronizedMapProgressCounter);
		testThreads(concurrentMap, concurrentMapProgressCounter);

	}

	private static void testThreads(final Map<String, String> sharedMap, final AtomicInteger progressCounter) {

		final List<Thread> threads = new ArrayList<>();

		for (int i = 0; i < 4; i++) {

			final int threadIdentifier = i;

			threads.add(new Thread(() -> {

				while (true) {

					for (int j = 0; j < 1_000; j++) {
						sharedMap.put(threadIdentifier + " - " + j, Integer.toString(j));
					}

					for (int j = 0; j < 1_000; j++) {
						sharedMap.remove(threadIdentifier + " - " + j);
					}

					progressCounter.incrementAndGet();
					System.out.print('\r');
					System.out.print("Current progress of synchronized map is: " + synchronizedMapProgressCounter.get() +
						", and of concurrent map is: " + concurrentMapProgressCounter.get());

				}

			}));

		}

		for (final Thread thread : threads) {
			thread.start();
		}

	}

}
