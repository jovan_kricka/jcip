package com.jcip.session3.blockingqueue;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

/**
 * <p>Consumer thread will consume messages from the queue until it consumes "poison pill" message.</p>
 *
 * @author Jovan Kricka
 * @since 2019-08-09.
 */
public class Consumer implements Runnable {

	private BlockingQueue<Message> queue;

	public Consumer(BlockingQueue<Message> q) {
		this.queue = q;
	}

	@Override
	public void run() {

		try {

			Message msg;

			//consuming messages until exit message is received
			while ((msg = queue.take()).getMsg() != "exit") {

				Thread.sleep(10);
				System.out.println("Consumed " + msg.getMsg());

			}

		} catch (InterruptedException e) {
			e.printStackTrace();
		}

	}

	public static void main(String[] args) {

		//Creating BlockingQueue of size 10
		BlockingQueue<Message> queue = new ArrayBlockingQueue<>(10);

		Producer producer = new Producer(queue);
		Consumer consumer = new Consumer(queue);

		//starting producer to produce messages in queue
		new Thread(producer).start();
		//starting consumer to consume messages from queue
		new Thread(consumer).start();

		System.out.println("Producer and Consumer has been started");

	}
}