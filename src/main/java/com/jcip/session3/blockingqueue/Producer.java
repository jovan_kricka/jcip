package com.jcip.session3.blockingqueue;

import java.util.concurrent.BlockingQueue;

/**
 * <p>Producer thread will produce 100 messages and put them to the queue.</p>
 * <p>When done it will put a "poison pill" to the queue.</p>
 *
 * @author Jovan Kricka
 * @since 2019-08-09.
 */
public class Producer implements Runnable {

	private BlockingQueue<Message> queue;

	public Producer(BlockingQueue<Message> q) {
		this.queue = q;
	}

	@Override
	public void run() {

		//produce messages
		for (int i = 0; i < 100; i++) {

			Message msg = new Message("" + i);

			try {

				Thread.sleep(i);
				queue.put(msg);
				System.out.println("Produced " + msg.getMsg());

			} catch (InterruptedException e) {
				e.printStackTrace();
			}

		}

		//adding exit message
		Message msg = new Message("exit");

		try {
			queue.put(msg);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

	}

}
