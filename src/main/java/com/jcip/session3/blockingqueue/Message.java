package com.jcip.session3.blockingqueue;

/**
 * @author Jovan Kricka
 * @since 2019-08-09.
 */
public class Message {

	private String msg;

	public Message(String str) {
		this.msg = str;
	}

	public String getMsg() {
		return msg;
	}

}
