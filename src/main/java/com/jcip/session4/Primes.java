package com.jcip.session4;

import static com.jcip.util.PrimesUtil.sumOfFirstNPrimes;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

/**
 * <p>We first create a {@link FutureTask} that should calculate sum of first N primes.</p>
 * <p>Then we dispatch that task to another thread.</p>
 * <p>And finally we wait for that task to be completed, and print out the result.</p>
 *
 * @author Jovan Kricka
 * @since 2019-08-09.
 */
public class Primes {

	private static final int NUMBER_OF_PRIMES = 50_000;

	public static void main(String[] args) throws ExecutionException, InterruptedException {

		System.out.println("Creating unit of work...");
		final FutureTask<Long> primesSum = new FutureTask<>(() -> sumOfFirstNPrimes(NUMBER_OF_PRIMES));
		System.out.println("Dispatching work to worker thread...");

		new Thread(primesSum).start();

		System.out.println("Waiting for work to be completed...");
		System.out.println("Sum of first " + NUMBER_OF_PRIMES + " primes is: " + primesSum.get());

	}

}
