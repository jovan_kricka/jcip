package com.jcip.session4;

import java.util.concurrent.CountDownLatch;

/**
 * @author Jovan Kricka
 * @since 2019-08-09.
 */
public class Latch {

    private static final int MAX_WHITE_WALKERS = 100;

    private static final CountDownLatch hodor = new CountDownLatch(MAX_WHITE_WALKERS);

    public static void main(String[] args) throws InterruptedException {

        for (int i = 0; i < MAX_WHITE_WALKERS; i++) {

            new Thread(() -> {

                synchronized (hodor) {

                    final String severity =
                            hodor.getCount() > 66 ? "This is a piece of cake..." : hodor.getCount() > 33 ? "Fuckers just keep coming.." : "Aaaaghh";

                    System.out.println("Holding door with " + (MAX_WHITE_WALKERS - hodor.getCount()) + " whitewalkers on them... " + severity);

                    try {
                        Thread.sleep((long) (1000 * Math.random()));
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    hodor.countDown();

                }

            }).start();

        }

        hodor.await();

        System.out.println();
        System.out.println("Hodor could not ho the dor anymore :(");

    }

}
