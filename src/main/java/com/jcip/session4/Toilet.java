package com.jcip.session4;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Semaphore;
import java.util.stream.Collectors;

/**
 * <p>{@link Toilet#PERSONS} are competing for the toilet but only {@link Toilet#TOILET_CAPACITY} person can be in the toilet at the same time.</p>
 * <p>Person goes into the toilet, drops the bomb, and then waits to write his total number of dumps on the board.</p>
 * <p>After finishing he sleeps. Inside or outside of the toilet?</p>
 *
 * @author Jovan Kricka
 * @since 2019-08-09.
 */
public class Toilet {

	private static final int TOILET_CAPACITY = 3;
	private static final String[] PERSONS = { "Jennifer", "Aykut", "Andrii", "JP", "Jovan", "Jasper", "Jorn", "Georgi", "Vlad", "Dogus", "Frederik" };

	private static final CountDownLatch latch = new CountDownLatch(PERSONS.length);
	private static final Semaphore toilet = new Semaphore(TOILET_CAPACITY);
	private static final Map<String, Integer> dumpsBoard = Arrays.stream(PERSONS).collect(Collectors.toConcurrentMap(person -> person, person -> 0));

	public static void main(String[] args) throws InterruptedException {

		for (int i = 0; i < PERSONS.length; i++) {

			final String person = PERSONS[i];
			new Thread(() -> {

				try {

					for (int j = 0; j < Math.random() * 100; j++) {

						toilet.acquire();

						Thread.sleep((long) (1000 * Math.random())); // dropping the bomb

						synchronized (dumpsBoard) {
							System.out
								.println(person + " just took a dump, and sharing toilet with " + (TOILET_CAPACITY - toilet.availablePermits() - 1) +
									" persons , while " + toilet.getQueueLength() + " persons are waiting outside.");
							dumpsBoard.put(person, dumpsBoard.get(person) + 1);
						}

						toilet.release();

						Thread.sleep(30);

					}

					latch.countDown();

				} catch (InterruptedException e) {
					e.printStackTrace();
				}

			}).start();

		}

		latch.await();

		final List<Entry<String, Integer>> dumpScore = dumpsBoard.entrySet()
			.stream()
			.sorted((e1, e2) -> e2.getValue() - e1.getValue())
			.collect(Collectors.toList());

		System.out.println();
		for (Entry<String, Integer> dumper : dumpScore) {
			System.out.println(dumper.getKey() + " took " + dumper.getValue() + ". dumps.");
		}

	}

	private static String getRandomElement(final Set<String> set) {

		int size = set.size();
		int item = new Random().nextInt(size); // In real life, the Random object should be rather more shared than this
		int i = 0;
		for (String obj : set) {
			if (i == item) {
				return obj;
			}
			i++;
		}

		return "John Doe";
	}

}
