package com.jcip.session4;

import java.util.List;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.CyclicBarrier;

public class Developers {

    private static final int NUMBER_OF_DEVELOPERS = 10;
    private static final long AMOUNT_OF_WORK = 100;

    private static final List<Long> results = new CopyOnWriteArrayList<>();
    private static volatile double releaseVersion = 0.1;
    private static final CyclicBarrier release = new CyclicBarrier(NUMBER_OF_DEVELOPERS, () -> {

        System.out.println("Version " + releaseVersion + " released.");
        releaseVersion+= 0.1;
        System.out.println(results);
        results.clear();

    });

    public static void main(String[] args) {

        for (int i = 0; i < NUMBER_OF_DEVELOPERS; i++) {

            final int threadIdentifier = i;
            final long restTime = Math.round(100 * Math.random());
            new Thread(() -> {

                while (true) {

                    long sum = 0;
                    for (int j = 0; j < AMOUNT_OF_WORK; j++) {

                        sum += j * 1_000_000 * Math.random();

                        try {
                            Thread.sleep(restTime);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }

                    }

                    results.add(sum);
                    System.out.println("I am developer " + threadIdentifier + " and 've done my work, back to the cat videos!");

                    try {
                        release.await();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } catch (BrokenBarrierException e) {
                        e.printStackTrace();
                    }

                }

            }).start();

        }

    }

}
