package com.jcip.util;

/**
 * @author Jovan Kricka
 * @since 2019-08-21.
 */
public class PrimesUtil {

	private PrimesUtil() {
	}

	public static long sumOfFirstNPrimes(final int n) {

		int crunchifyStartNumber = 2;
		int counter = 1;
		long crunchifySum = 0;

		while (!Thread.currentThread().isInterrupted() && counter <= n) {
			if (checkIfPrimeNumber(crunchifyStartNumber)) {
				crunchifySum += crunchifyStartNumber;
				counter++;
			}
			crunchifyStartNumber++;
			if (crunchifyStartNumber % 10_000 == 0) {
				System.out.print(".");
			}
		}

		System.out.println(Thread.currentThread().isInterrupted() ? "I wanted to get shit done, but I was interrupted :(" : "I am done!");

		return crunchifySum;

	}

	private static boolean checkIfPrimeNumber(int primeNumber) {

		for (int i = 2; i <= primeNumber / 2; i++) {
			if (primeNumber % i == 0) {
				return false; // Nope.. it's not a prime number
			}
		}
		return true; // Yup.. it's prime number

	}

}
